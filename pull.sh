#!/bin/bash
project_id="30800621"
wasmpath="hello"
wasmfile="hello.wasm"
version="0.0.0"
package_name="hello-sat"
curl "https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}" \
     --output ${wasmfile}

```bash
sat "$(gp url 3000)/hello/hello.wasm"
```