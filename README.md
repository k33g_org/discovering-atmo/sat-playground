# Sat Playground
> 🚧 this README is a work in progress

## What is Sat?

**[Sat](https://github.com/suborbital/sat)** (from **[Suborbital](https://suborbital.dev/)**) is the simplest (probably the smallest) way to serve a **wasm microservice** 🚀

## About this project

This project is a Playground with all that you need to start playing with **Sat** right now without installing anything. So, **to get the benefit of that, you have to open this project with [Gitpod](https://www.gitpod.io/)**.

### 🤚 To begin, click on the **Gitpod** button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/discovering-atmo/sat-playground)


## How to use this playground?

### 1️⃣ [Step 01] Create and build a Runnable

```bash
subo create runnable hello
subo build ./hello
```
> - the 1st command will generate a Rust project, have a look 👀 to the source code: `./hello/src/lib.rs`
> - the 2nd command will generate a wasm file: `./hello/hello.wasm`

## 2️⃣ [Step 02] Start Sat and serve the Runnable

```bash
sat "$(gp url 3000)/hello/hello.wasm"
```
> - the command will start **Sat**, then **Sat** will download the wasm file
> - more info here: https://github.com/suborbital/sat#run-from-url

## 3️⃣ [Step 03] Call the Runnable service

```bash
echo 'Jane Doe' | http $(gp url 8080)  Content-Type:text/plain
```
> httpie is already installed

That's all for today 🎉 anf have fun 😄

