#!/bin/bash
project_id="30800621"
wasmpath="hello"
wasmfile="hello.wasm"
version="0.0.0"
package_name="hello-sat"

#docker run -it -e SAT_HTTP_PORT=8080 -p 8080:8080 suborbital/sat:latest \

# git clone https://github.com/suborbital/sat.git
# cd sat
# make sat


SAT_HTTP_PORT=8080 sat/.bin/sat https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}

